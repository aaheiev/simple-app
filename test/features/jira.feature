Feature: Jira Test instance is up and running
  In order to be sure Jira Test instance is up and running
  As a anonymous user
  I want to see the if it works on JIRAs search page

  Scenario: View home page
    Given I am on the home page
    When I fill in "quickSearchInput" with "something"
    Then I should see "No issues were found to match your search"

  Scenario: Login to JIRA
    Given I am on the login page
    When I fill in login with "admin" and in password with "P@ssw0rd" and click LogIn
    Then I should see "System Dashboard"

  # Scenario: Create JIRA Issue
  #   Given I am on the Dashboard page
  #   When I create issue with summary "Test-issue173"
  #   Then I should see "Test-issue173 has been successfully created"
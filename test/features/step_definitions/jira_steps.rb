Given(/^I am on the home page$/) do
  visit "https://jira.nest-devinfra-m.nestlabs.com"
end

Given(/^I am on the login page$/) do
  visit "https://jira.nest-devinfra-m.nestlabs.com/login.jsp"
end

When /^I fill in "([^"]*)" with "([^"]*)"$/ do |element, text|
  visit 'https://jira.nest-devinfra-m.nestlabs.com/secure/Dashboard.jspa'
  fill_in element, with: text
  find('#quickSearchInput').native.send_keys :enter
end

When(/^I fill in login with "([^"]*)" and in password with "([^"]*)" and click LogIn$/) do |username, password|
  visit 'https://jira.nest-devinfra-m.nestlabs.com/login.jsp'
  fill_in 'os_username', with: username
  fill_in 'os_password', with: password
  find('#login-form-password').native.send_keys :enter
end

Then(/^I should see "(.*?)"$/) do |text|
  page.should have_content text
  #page.driver.resize(20,30)
  # page.save_screenshot("test.pdf")
  #puts page.within_window
  # puts page.driver.network_traffic
  #puts page.driver.cookies
  #puts page.response_headers.to_a 
end

Given(/^I am on the Dashboard page$/) do
  visit 'https://jira.nest-devinfra-m.nestlabs.com/secure/Dashboard.jspa'
end

When(/^I create issue with summary "([^"]*)"$/) do |summary|
  fill_in 'os_username', with: 'admin'
  fill_in 'os_password', with: 'P@ssw0rd'
  find('#login-form-password').native.send_keys :enter
  
  click_link 'Create'
  fill_in 'Summary', with: summary
  find('#summary').native.send_keys :enter
  # click_link 'Create'
end


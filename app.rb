require 'sinatra'
require "rdiscount"
require 'yaml'
require 'json'
require 'logger'
require 'faker'
require 'logger'
require 'date'

# set Sinatra vars
set :root,   File.dirname(__FILE__)
set :logger, Logger.new(STDOUT)
set :bind,   '0.0.0.0'

config = YAML.load_file('config.yaml')
config['app_title'] = ENV['APP_TITLE'] if ENV['APP_TITLE']

# get product version from file
product_version = File.read "VERSION"
if ENV['BUILD_VERSION']
  assembly_version = ENV['BUILD_VERSION']
else
  assembly_version = product_version
end

get '/' do
  @app_title        = config['app_title']
  @assembly_version = assembly_version
  @contact_email    = Faker::Internet.email
  @contact_phone    = Faker::PhoneNumber.phone_number
  erb :index
end

get '/status.json' do
  status = {status: 'ok'}
  if status[:healty]
    content_type :json
    status.to_json
  else
    status 500
    content_type :json
    body status.to_json
  end
end

get '/status' do
  "ok"
end

get '/version' do
  assembly_version
end

get '/run' do
  100.times do |i|
    100000.downto(1) do |j|
      Math.sqrt(j) * i / 0.2
    end
  end
  "done."
end

not_found do
  status 404
  "Error: 404 - requested resource not found."
end

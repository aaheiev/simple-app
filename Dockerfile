# docker build -t ngs-simple-app .
FROM ruby:2.6

ARG assembly_version=0.0.0.0
ARG http_port=9292
ARG branch_name='undefined'

ENV GIT_BRANCH_NAME    $branch_name
ENV BUILD_VERSION      $assembly_version
ENV HTTP_PORT          $http_port
ENV HEALTHCHECK_URL    "http://127.0.0.1:${http_port}/status"

RUN mkdir /app

WORKDIR /app

COPY . /app/

RUN bundle install

EXPOSE $HTTP_PORT

CMD bundle exec rackup -o 0.0.0.0 -p $HTTP_PORT

HEALTHCHECK --interval=60s --timeout=5s --retries=12 CMD curl --silent --fail $HEALTHCHECK_URL || exit 1
